import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { MainPageComponent } from './app/users-view/main-page/main-page.component';
import { AdminMainViewComponent } from './app/admin-view/admin-main-view/admin-main-view.component';

const appRoutes: Routes = [
    { path: '', redirectTo: 'homepage', pathMatch: 'full' },
    { path: 'homepage', component : MainPageComponent },
    { path: 'admin', component : AdminMainViewComponent },
];

@NgModule({
    imports: [RouterModule.forRoot(appRoutes)],
    exports: [RouterModule],
})
export class AppRoutingModule {}
