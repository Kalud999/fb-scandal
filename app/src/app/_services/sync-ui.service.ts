import { Injectable } from '@angular/core';
import { BehaviorSubject, Subject } from 'rxjs';
import { FilterByDateQuery } from '../users-view/main-page/scandals/_pipes/date-filter.pipe';

export  enum direction { UP , DOWN }

@Injectable({
  providedIn: 'root'
})

export class SyncUIService { //°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°

  constructor() { }

  showScrollToTopButton$  = new Subject<boolean>() ;
  scrollToTopNow$  = new Subject<boolean>() ;

  moveFilterUp$  = new Subject<direction>() ;  //controls the loading icon
  blurEverything$  = new Subject<boolean>() ;

  filterByNameSearchQuery$ = new BehaviorSubject<string>('');
  filterByDateQuery$ = new BehaviorSubject<FilterByDateQuery>(null);


} //°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°
