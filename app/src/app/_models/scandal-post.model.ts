export interface ScandalPost {
    date: Date;
    title: string;
    description: string;
    sources: ScandalSource[];
}

export interface ScandalSource {
    name: string;
    url: string;
    title: string;
    date: Date;
}

