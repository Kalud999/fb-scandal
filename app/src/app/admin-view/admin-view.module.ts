import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdminMainViewComponent } from './admin-main-view/admin-main-view.component';


@NgModule({
  declarations: [AdminMainViewComponent],
  imports: [
    CommonModule
  ]
})
export class AdminViewModule { }
