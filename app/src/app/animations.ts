import { animate, style, transition, trigger } from '@angular/animations';

export const scandalPostPopupTrigger = trigger('scandalPostPopupState' , [
    transition(':enter' , [
        style({ transform : 'scale(0.8)' , opacity : 0.5 }),
        animate('0.3s ease-in-out' , style({
            transform : 'scale(1.05)',
            opacity : 1,
        })),
        animate('0.1s ease-out' , style({
            transform : 'scale(1)',
        })),
    ]),
])



