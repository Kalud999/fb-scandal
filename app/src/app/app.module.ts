import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { UsersViewModule } from './users-view/users-view.module';
import { AppRoutingModule } from '../app-routing-module';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
    declarations: [
        AppComponent,
    ],
    imports: [
        BrowserModule,
        UsersViewModule,
        AppRoutingModule,
        HttpClientModule,
    ],
    providers: [],
    exports: [],
    bootstrap: [AppComponent],
})

export class AppModule { }
