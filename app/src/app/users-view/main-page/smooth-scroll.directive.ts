import { Directive, ElementRef, HostListener } from '@angular/core';
import { SimpleSmoothScrollService } from 'ng2-simple-smooth-scroll';
import { SyncUIService } from '../../_services/sync-ui.service';

@Directive({
  selector: '[appSmoothScroll]'
})
export class SmoothScrollDirective {  //°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°

  private targetElement: HTMLElement;

  constructor(private element   : ElementRef,
              private smooth    : SimpleSmoothScrollService,
              private syncUIService: SyncUIService) {}


  ngOnInit(): void {
    this.syncUIService.scrollToTopNow$.subscribe(data => {
      if(data) {
        this.targetElement =  this.element.nativeElement;
        this.smooth.smoothScroll(0, { duration: 500, easing: 'easeOutCubic' }, this.targetElement);
      }
    });
  }



}  //°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°
