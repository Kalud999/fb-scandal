import { Directive, ElementRef, Input, Renderer2 } from '@angular/core';

@Directive({
  selector: '[appTimelineCirclePos]'
})

//°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°
export class TimelineCirclePosDirective {
  @Input() index:number;

  constructor(private layer : ElementRef , private renderer : Renderer2) {}

  ngOnInit() {
    const targetElement  = this.layer.nativeElement;
    this.renderer.setStyle(targetElement, 'grid-row' , `${this.index + 1} / ${this.index + 2}`);
  }

} //°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°
