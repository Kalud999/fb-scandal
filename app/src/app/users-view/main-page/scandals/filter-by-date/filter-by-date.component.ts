import { ChangeDetectorRef, Component, Input, OnInit } from '@angular/core';
import { SyncUIService } from '../../../../_services/sync-ui.service';
import { ScandalPost } from '../../../../_models/scandal-post.model';
import { FilterByDateQuery, Month, Year } from '../_pipes/date-filter.pipe';


export interface yearObject {
    year: number,
    months: Month[]
}

@Component({
  selector: 'app-filter-by-date',
  templateUrl: './filter-by-date.component.html',
  styleUrls: ['./filter-by-date.component.scss']
})
export class FilterByDateComponent implements OnInit { //°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°
    public yearsObjects: yearObject[] = [
        // { year: 2022 , months: [ 'Feb' , 'Jan' , 'Dec'] },
        // { year: 2021 , months: [ 'Mar' , 'Jul' , 'Oct'] },
        // { year: 2019 , months: [ 'Mar' , 'Jul' , 'Oct'] }
    ]

    currentlySelectedDate: FilterByDateQuery = { year : null, month: null };

    hiddenYears: number[] = [];

    allPosts: ScandalPost[];
    @Input() set postsSetter(posts: ScandalPost[]) {
        this.allPosts = posts;
        this.fillYearsObjects();
    }

    constructor(private cdr: ChangeDetectorRef, private syncUIService: SyncUIService) {
    }



  ngOnInit(): void {
        this.yearsObjects.forEach(el => {
            this.hiddenYears.push(el.year);
        });
  }


    toggleMonths(obj: any) {
        let clickedYear = obj.year;
        let indexOfClickedYear = this.hiddenYears.indexOf(clickedYear);
        if(indexOfClickedYear === -1) { this.hiddenYears.push(clickedYear) }
        else { this.hiddenYears.splice(indexOfClickedYear, 1) }
    }


    showMonths(year: number) {
        let isTheYearAlreadyHidden;

        if(this.hiddenYears.length > 0) {
            isTheYearAlreadyHidden = false;
            this.hiddenYears.forEach(el => {
                if(el === year) { isTheYearAlreadyHidden = true }
            });
        } else {
            isTheYearAlreadyHidden = false;
        }

        return !isTheYearAlreadyHidden;
    }


    yearClicked(obj: any) {
        // this.toggleMonths(obj);

        let clickedYear = obj.year;
        this.currentlySelectedDate.month = null;

        if(this.currentlySelectedDate.year === clickedYear) {
            this.currentlySelectedDate.year = null;
            this.syncUIService.filterByDateQuery$.next({ year: null , month: null });
        }
        else {
            this.currentlySelectedDate.year = clickedYear;
            this.syncUIService.filterByDateQuery$.next({ year: clickedYear , month: null });
        }

    }

    monthClicked(year: number, month: Month) {
        this.currentlySelectedDate.year = year;

        if(this.currentlySelectedDate.month === month) {
            this.currentlySelectedDate.month = null;
            this.syncUIService.filterByDateQuery$.next({ year , month: null });
        }
        else {
            this.currentlySelectedDate.month = month;
            this.syncUIService.filterByDateQuery$.next({ year , month });
        }


    }


    private fillYearsObjects() {
        if(this.allPosts.length > 0) {
            this.allPosts.forEach((post, i ) => {
                let dateObj = new Date(post.date);
                if(isYearAlreadySaved(dateObj.getFullYear(), this.yearsObjects)) {
                    this.yearsObjects.forEach(obj => {
                        if(obj.year === dateObj.getFullYear()) {
                            addMonthIfNotAlreadySaved(obj, Month[dateObj.getMonth()] as any);
                        }
                    })
                }
                else {
                    let yearObject = { year: null,  months: [] } as yearObject;
                    yearObject.year = dateObj.getFullYear();
                    yearObject.months.push(Month[dateObj.getMonth()] as any);
                    this.yearsObjects.push(yearObject);
                }

            });
        }


        function isYearAlreadySaved(year: number, yearsObjects: yearObject[]): boolean {
            let result = false;
            yearsObjects.forEach(obj => { if(obj.year === year) { result = true } });

            return result;
        }

        function addMonthIfNotAlreadySaved(obj: yearObject, month: Month) {
            if(obj.months.indexOf(month) === -1) { obj.months.push(month) }
        }

    } //end of fillYearsObjects



} //°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°
