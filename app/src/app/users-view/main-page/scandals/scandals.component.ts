import { Component, HostListener, OnInit } from '@angular/core';
import { ScandalPost } from '../../../_models/scandal-post.model';
import { AjaxService } from '../../../_services/ajax.service';
import { direction, SyncUIService } from '../../../_services/sync-ui.service';
import { scandalPostPopupTrigger } from '../../../animations';

@Component({
  selector: 'app-scandals',
  templateUrl: './scandals.component.html',
  styleUrls: ['./scandals.component.scss'],
  animations : [
    scandalPostPopupTrigger,
  ],
})
export class ScandalsComponent implements OnInit { //°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°
  scandalsList: ScandalPost[] = [];
  adjustFilterPosition = false;

  currentScandalPost: ScandalPost;
  isScandalPostOpened = false;

  constructor(private ajaxService: AjaxService, public syncUIService: SyncUIService) {
    this.syncUIService.moveFilterUp$.subscribe(dir => {
      if(dir === direction.UP) { this.adjustFilterPosition = true }
      else if(dir === direction.DOWN) {  this.adjustFilterPosition = false }
    });
  }


  @HostListener('document:click', ['$event']) onClick(event) {
    if(event.target.className.includes('displayScandalCard') || event.target.className.includes('closeButton')) {
      this.isScandalPostOpened = false;
    }
  }

  @HostListener('document:keydown', ['$event']) handleKeyboardEvent(event: KeyboardEvent) {
    if(event.code === 'Escape') { this.isScandalPostOpened = false; }
  }




  ngOnInit(): void {
    this.ajaxService.fetchScandalsFromFirebase().subscribe((list:any) => {
      this.scandalsList = list;
      this.currentScandalPost = this.scandalsList[15]
    });
  }


  postClicked(post: ScandalPost, event: any) {
    console.log(event);
    event.stopPropagation();

    this.currentScandalPost = post;
    this.isScandalPostOpened = true;
    // this.syncUIService.blurEverything$.next(true); //it didnt work !!!
  }


} //°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°
