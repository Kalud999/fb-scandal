import { Directive, HostListener } from '@angular/core';
import { direction, SyncUIService } from '../../../../_services/sync-ui.service';

@Directive({
  selector: '[appDetectScroll]'
})
export class DetectScrollDirective {  //°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°

  constructor(private syncUIService : SyncUIService) {
  }

  @HostListener("scroll", ["$event"]) onListenerTriggered(event): void {
    const percent = Math.round((event.srcElement.scrollTop / (event.srcElement.scrollHeight - event.srcElement.clientHeight)) * 100);

    if(percent >= 3 && percent <= 20) {
     this.syncUIService.moveFilterUp$.next(direction.UP);
    }

    if(percent < 3 &&  percent >= 0) {
      this.syncUIService.moveFilterUp$.next(direction.DOWN);
    }

    if(percent > 6) {
      this.syncUIService.showScrollToTopButton$.next(true);
    }

    if(percent < 6) {
      this.syncUIService.showScrollToTopButton$.next(false);
    }

  }




} //°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°
