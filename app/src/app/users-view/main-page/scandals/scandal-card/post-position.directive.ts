import { Directive, ElementRef, Input, Renderer2 } from '@angular/core';

@Directive({
  selector: '[appPostPosition]'
})

//°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°
export class PostPositionDirective {
  @Input() index:number;

  constructor(private layer : ElementRef , private renderer : Renderer2) {}

  ngOnInit() {
    const targetElement  = this.layer.nativeElement;
    console.log(this.index);
    console.log(targetElement);

    let whichColumn: string;

    if(this.index % 2 === 0) { whichColumn = '1 / 2' }
    else { whichColumn = '3 / 4' }

    this.renderer.setStyle(targetElement, 'grid-column', whichColumn);
    this.renderer.setStyle(targetElement, 'grid-row' , `${this.index + 1} / ${this.index + 2}`);
  }


}  //°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°
