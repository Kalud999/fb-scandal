import { Component, Input, OnInit } from '@angular/core';
import { ScandalPost } from '../../../../_models/scandal-post.model';

@Component({
  selector: 'app-scandal-card',
  templateUrl: './scandal-card.component.html',
  styleUrls: ['./scandal-card.component.scss']
})

//°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°
export class ScandalCardComponent implements OnInit {
  scandal: ScandalPost;
  showScandal = false;

  @Input() opened: boolean = false;

  @Input() postIndex: number;
  @Input() set scandalSetter(scandal: ScandalPost) {
    this.scandal = scandal;
    this.showScandal = true;
  }

  constructor() { }

  ngOnInit(): void {
  }

    openLinkInNewTab(url: string) {
      if(this.showScandal) { window.open(url, '_blank') }
    }


} //°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°
