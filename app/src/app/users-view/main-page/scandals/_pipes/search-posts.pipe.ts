import { Pipe, PipeTransform } from '@angular/core';
import { ScandalPost } from '../../../../_models/scandal-post.model';

@Pipe({
  name: 'searchPosts'
})
export class SearchPostsPipe implements PipeTransform { //°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°

  transform(posts: ScandalPost[], query : string): ScandalPost[] {
    if(!query || query.length === 0) { return posts }

    const finalArray: ScandalPost[] = [];

    const queryCaseSmall = query.toLowerCase();

    posts.forEach(post => {
      if( post.title.toLowerCase().includes(queryCaseSmall)
          || post.description.toLowerCase().includes(queryCaseSmall)) {
            finalArray.push(post);
          }
    });

    return finalArray;
  }


} //°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°
