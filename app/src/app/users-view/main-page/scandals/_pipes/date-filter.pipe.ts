import { Pipe, PipeTransform } from '@angular/core';
import { ScandalPost } from '../../../../_models/scandal-post.model';


export type Year = number;
export enum Month  {
  Jan,
  Feb,
  Mar,
  Apr,
  May,
  June,
  July,
  Aug,
  Sept,
  Oct,
  Nov,
  Dec,
}

export interface FilterByDateQuery  {
  year : number;
  month: Month;
}

@Pipe({
  name: 'dateFilter'
})
export class DateFilterPipe implements PipeTransform { //°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°
  finalArray: ScandalPost[] = [];

  transform(posts: ScandalPost[], query: FilterByDateQuery): ScandalPost[] {
    if(query === null || undefined) { return posts }
    this.finalArray = [];

    console.log(query);

    if(query.year === null && query.month === null ) { this.finalArray = posts }
    else if(query.month === null) { this.showAllMonthsOfYear(query.year, posts); }
    else {
      posts.forEach(post => {
        let dateObj = new Date(post.date);
        if(dateObj.getFullYear() === query.year) {
          if(Month[dateObj.getMonth() as any] === query.month as any) {
            this.finalArray.push(post)
          }
        }
      });
    }

    return this.finalArray;
  }


  private showAllMonthsOfYear(year: number, posts: ScandalPost[]) {
    posts.forEach(post => {
      let dateObj = new Date(post.date);
      if(dateObj.getFullYear() === year) { this.finalArray.push(post) }
    });
  }


} //°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°
