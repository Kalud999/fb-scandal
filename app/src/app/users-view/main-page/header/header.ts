import { Component, OnInit } from '@angular/core';
import { SyncUIService } from '../../../_services/sync-ui.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.html',
  styleUrls: ['./header.scss']
})
export class Header implements OnInit {  //°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°

  constructor(private syncUIService: SyncUIService) { }

  ngOnInit(): void {
  }

    navBarItemClick(event: MouseEvent) {
        console.log(event);
    }


    typingSearchQuery(value: string) {
      this.syncUIService.filterByNameSearchQuery$.next(value);
    }


} //°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°

