import { Component, OnInit } from '@angular/core';
import { SyncUIService } from '../../_services/sync-ui.service';
import { SimpleSmoothScrollService } from 'ng2-simple-smooth-scroll';


@Component({
  selector: 'app-main-page',
  templateUrl: './main-page.component.html',
  styleUrls: ['./main-page.component.scss']
})

//°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°
export class MainPageComponent  {
  blurEverything = false;
  showScrollToTopBottom = false;

  constructor(public syncUIService: SyncUIService, private smoothScrollService : SimpleSmoothScrollService) {
    this.syncUIService.blurEverything$.subscribe(data => {
      this.blurEverything = data;

      console.log('we got order : ' , data);
      this.showScrollToTopBottom = data;
    });
  }


  goToTop() {
    this.syncUIService.scrollToTopNow$.next(true);
    // this.smoothScrollService.smoothScroll(scrollTop, { duration: 500, easing: 'easeInOutQuint' }, this.targetElement);
  }



} //°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°°



// getDate(dateAsString: any): Date {
//   // var gg = new Date('21-04-2021')
//   if(dateAsString?.includes('/')) {
//     const dateSplit = <string>dateAsString.split('/');
//     const day: number = +dateSplit[0];
//     const month: number = (+dateSplit[1]) - 1;
//     const year: number = +dateSplit[2];
//
//     const ff = new Date(year, month , day);
//     return ff;
//   }
//   else {
//     return dateAsString;
//   }
//
// }
