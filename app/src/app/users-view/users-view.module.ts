import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MainPageComponent } from './main-page/main-page.component';
import { ScandalCardComponent } from './main-page/scandals/scandal-card/scandal-card.component';
import { PostPositionDirective } from './main-page/scandals/scandal-card/post-position.directive';
import { TimelineCirclePosDirective } from './main-page/timeline-circle-pos.directive';
import { LastScandalBannerComponent } from './main-page/last-scandal-banner/last-scandal-banner.component';
import { Header } from './main-page/header/header';
import { ScandalsComponent } from './main-page/scandals/scandals.component';
import { FilterByDateComponent } from './main-page/scandals/filter-by-date/filter-by-date.component';
import { DetectScrollDirective } from './main-page/scandals/_directives/detect-scroll.directive';
import { SearchPostsPipe } from './main-page/scandals/_pipes/search-posts.pipe';
import { DateFilterPipe } from './main-page/scandals/_pipes/date-filter.pipe';
import { AdjustDateFilterPositionDirective } from './main-page/scandals/_directives/adjust-date-filter-position.directive';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SmoothScrollDirective } from './main-page/smooth-scroll.directive';


@NgModule({
  declarations: [
      MainPageComponent,
      ScandalCardComponent,
      PostPositionDirective,
      TimelineCirclePosDirective,
      LastScandalBannerComponent,
      Header,
      ScandalsComponent,
      FilterByDateComponent,
      DetectScrollDirective,
      SearchPostsPipe,
      DateFilterPipe,
      AdjustDateFilterPositionDirective,
      SmoothScrollDirective,
  ],
    imports: [
        BrowserAnimationsModule,
        CommonModule,
    ],
    exports: [
        DetectScrollDirective,
    ],
})

export class UsersViewModule { }
